<%-- 
    Document   : registration
    Created on : 18.10.2021, 19:06:53
    Author     : shubin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
        <div id="main">
            <aside class="leftAside">
                <h2>Что нужно для регистрации</h2>
                <p>
                    Чтобы регистрация прошла успешно, заполните все поля и нажмите на кнопку "Зарегестрироваться"
                </p>
            </aside>
            <section>
                <article>
                    <h1>Регистрация</h1>
                    <div class="text-article">
                        <form method="POST" action="registration">
                            <p>
                                <label for="login">Логин</label>
                                <input type="text" name="login" id="login"/>
                            </p>
                            <p>
                                <label for="email">E-Mail</label>
                                <input type="email" name="email" id="email"/>
                            </p>
                            <p>
                                <label for="password">Пароль</label>
                                <input type="password" name="password" id="password"/>
                            </p>
                            <p>
                                <label for="password2">Повторите пароль</label>
                                <input type="password2" name="password2" id="password2"/>
                            </p>
                            <p>
                                <button type="submit">Зарегестрироваться</button>
                            </p>
                        </form>
                    </div>
                </article>
            </section>
        </div>
