<%-- 
    Document   : article
    Created on : 18.10.2021, 19:05:56
    Author     : shubin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
        <div id="main">
            <aside class="leftAside">
                <h2>Новые лекарства</h2>
                <ul>
                    <li><a href="#">Лекарство 1</a></li>
                    <li><a href="#">Лекарство 2</a></li>
                    <li><a href="#">Лекарство 3</a></li>
                    <li><a href="#">Лекарство 4</a></li>
                </ul>
            </aside>
            <section>
                <article>
                    <h1>Рецепт</h1>
                    <div class="text-article">
                        Текст о лекарстве
                    </div>
                    <div class="fotter-article">
                        <span class="author">Автор лекарства: <a href="#">автор</a></span>
                        <span class="date-article">Дата выхода: 20.10.2021</span>
                    </div>
                </article>
            </section>
        </div>
